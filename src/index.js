// const element = document.createElement('h1');
// element.innerText = 'Hello, Platzi Badges!';

// const container = document.getElementById('app');

// container.appendChild(element);

import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';

import './global.css';


import App from './components/App'

//const element = <h1>Hello, Platzi Badges en React!</h1>;

//const element = React.createElement('h1',{}, 'Soy un children')
//const name = 'Raul'
//const element = React.createElement('h1',{},`Hola, mi nombre es: ${name}`)

//jsx
/*const jsx = (
    <div>
        <h1>Hola soy Raul</h1>
        <p>Ingeniero en sistemas</p>
    </div>
);

// react element
const element = React.createElement(
    'div',
    {},
    React.createElement('h1',{}, 'Hola soy Raul'),
    React.createElement('p',{},'Ingeniero en sistemas')

);*/

const container = document.getElementById('app');

ReactDOM.render(<App />, container);
// ReactDOM.render(__qué__, __dónde__);
/*ReactDOM.render(<Badge 
    firstname="Lilly" 
    lastname="Kaufmann"
    jobTitle="Ingineer Systems"
    redSocial="@lilly"

    />, container);*/
