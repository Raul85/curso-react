import React from 'react';

class BadgeForm extends React.Component {
   // state = {};

    handleChange = e => {
        /*console.log({
            name: e.target.name,
            value: e.target.value,
           
        });*/
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleClick = e => {
        console.log('Boton has click');
    };

  /*  handleSubmit = e => {
        e.preventDefault();
        console.log("Boton submitted");
        console.log(this.state);
    };*/

    render(){
        return (
            <div>
               
                <form onSubmit={this.props.onSubmit}>
                    <div className="form-group">
                        <label>Nombre </label>
                        <input 
                            onChange={this.props.onChange} 
                            className="form-control" 
                            type="text" 
                            name="firstName"
                            value={this.props.formValues.firstName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Apellido </label>
                        <input 
                            onChange={this.props.onChange} 
                            className="form-control" 
                            type="text" name="lastName"
                            value={this.props.formValues.lastName}
                        />
                    </div>
                    <div className="form-group">
                        <label>Email </label>
                        <input 
                            onChange={this.props.onChange} 
                            className="form-control" 
                            type="email" 
                            name="email" 
                            value={this.props.formValues.email}
                        />
                    </div>
                    <div className="form-group">
                        <label>Puesto </label>
                        <input 
                            onChange={this.props.onChange} 
                            className="form-control" 
                            type="text" 
                            name="jobTitle"
                            value={this.props.formValues.jobTitle}
                        />
                    </div>
                    <div className="form-group">
                        <label>Red Social </label>
                        <input 
                            onChange={this.props.onChange} 
                            className="form-control" 
                            type="text" 
                            name="redSocial"
                            value={this.props.formValues.redSocial}
                        />
                    </div>
                    <button onClick={this.handleClick} className="btn btn-primary">Guardar</button>
                    {this.props.error && (
                        <p className="text-danger">{this.props.error.message}</p>
                    )}
                </form>
            </div>
        )
    }

}

export default BadgeForm;