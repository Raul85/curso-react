import React from 'react';

import "./styles/Badge.css"
import confLogo from '../images/badge-header.svg';

class Badge extends React.Component {
    render() {
        return (
        <div className="Badge">
            <div className="Badge__header">
                <div>
                    <img src={confLogo} alt="Logo de conferencia" />
                </div>
            </div> 
            <div className="Badge__section-name">
                <div>
                    <img className="Badge__avatar" src="https://www.gravatar.com/avatar/f63a9c45aca0e7e7de0782a6b1dff40b?d=identicon" alt="Avatar"  />
                </div>
                <h1>{this.props.firstname} <br/> {this.props.lastname}</h1>
            </div>
            
            <div className="Badge__section-info">
                <h3>{this.props.jobTitle}</h3>
                
                <div>
                    {this.props.redSocial}
                </div>
            </div>
            <div className="Badge__footer">
            
            </div>
        </div>
        );
    }
}

export default Badge;