import React from 'react';

import './styles/BadgeNew.css';
import header from '../images/platziconf-logo.svg';

import BadgeForm from '../components/BadgeForm';
import Badge from '../components/badge';
import PageLoading from '../components/PageLoading';
import api from '../api'

class BadgeNew extends React.Component {
    state = { 
        loading: false,
        error: null,
        form: {
            firstName: '',
            lastName: '',
            email: '',
            jobTitle: '',
            redSocial: '',
        },    
    };

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            },
        });
    };

    handleSubmit = async e => {
        e.preventDefault()   
        this.setState({ loading: true, error: null });

        try {
            await api.badges.create(this.state.form);
            this.setState({ loading: false });

            this.props.history.push('/badges');
        } catch (error) {
            this.setState({ loading: false, error: error });
        }
    }


    render() {
        if (this.state.loading){
            return <PageLoading />;
        }
        return (
            <React.Fragment>
                
                <div className="BadgeNew__hero">
                    <img className="BadgeNew__hero-image  img-fluid" src={header} alt="Logo" />

                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <Badge 
                                firstname={this.state.form.firstName || 'FIRST_NAME'} 
                                lastname={this.state.form.lastName || 'LAST_NAME'}
                                redSocial={this.state.form.redSocial || 'RED SOCIAL'}
                                jobTitle={this.state.form.jobTitle || 'PUESTO'}
                                email={this.state.form.email || 'EMAIL'} 
                            />
                        </div>
                        <div className="col-6">
                            <h1>Nuevo Asistente</h1>
                            <BadgeForm 
                            onChange={this.handleChange} 
                            onSubmit={this.handleSubmit}
                            formValues={this.state.form} 
                            error={this.state.error}
                                                        
                            />
                        </div>
                    </div>
                </div>
            </React.Fragment>
            
        );
        
    }
}

export default BadgeNew;